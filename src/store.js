/*
This file is part of LiberaForms

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { defineStore } from "pinia";
import { useI18n } from "vue-i18n"
import axios from 'axios';

export const wizardStore = defineStore("wizardStore", {
  state: () => ({
    loading_source: true,
    data_endpoint: null,
    organization_endpoint: null,
    data_law: "",
    consent_id: null,
    consent_url: null,
    settings: {},
    default_organization: {},
    ui_language: "en",
    consent_language: "en",
    saved_organization_profile: false,
    questions: {
      "data_purpose": {"label": "Purpose", "component_name": "DataPurpose"},
      "legitimate_use": {"label": "Consent", "component_name": "LegitimateUse"},
      "my_organization": {"label": "My organization", "component_name": "MyOrganization"},
      "data_recipient": {"label": "Data recipient", "component_name": "DataRecipient"},
    },
    question_id: "data_purpose",
    wizard: {
      "is_legitimate_use": false,
      "legitimacy": "",
      "purpose": "",
      "my_organization": {
        "name": "",
        "email": "",
        "url": "",
        "is_public_administration": false,
      },
      "recipient_is_my_organization": true,
      "data_recipient": {
        "name": "",
        "email": "",
        "url": ""
      }
    }
  }),
  getters: {
    question_help: (state) => {
      const {t} = useI18n();
      return {"label": state.questions[state.question_id].label,
              "help_text": t(state.question_id)}
    },
    isAnswered: (state) => {
      return (question_id) => {
        if (question_id == "my_organization") {
          return state.wizard.my_organization.name ? true : false
        }
        if (question_id == "data_recipient") {
          if (state.wizard.recipient_is_my_organization) {
            if (state.wizard.my_organization.name) {
              return true
            }
          } else if (state.wizard.data_recipient.name) {
            return true
          }
          return false
        }
        if (question_id == "legitimate_use") {
          if (state.wizard.is_legitimate_use == true && state.wizard.legitimacy) {
            return true
          }
          if (state.wizard.is_legitimate_use === false) {
            return true
          }
          return false
        }
        if (question_id == "data_purpose") {
          return state.wizard.purpose ? true : false
        }
      }
    },
    canCopyResult: (state) => {
      if (state.wizard.recipient_is_my_organization == false &&
          !(state.wizard.data_recipient.name ||
            state.wizard.data_recipient.email ||
            state.wizard.data_recipient.url)) {
        return false
      }
      if (state.wizard.is_legitimate_use == true && !state.wizard.legitimacy) {
        return false
      }
      if (! state.wizard.purpose) {
        return false
      }
      if (state.wizard.my_organization.name) {
        return true
      }
      return false
    },
    result: (state) => {
      const {t} = useI18n();

      function addOrganization(organization) {
        var org = ""
        if (organization.name) {
          org = org + " " + organization.name + "."
        }
        if (organization.email) {
          org = org + " " + t("Email", 1, { locale: state.consent_language }) + ": " + organization.email + "."
        }
        if (organization.url) {
          org = org + " " + t("Website", 1, { locale: state.consent_language }) + ": " +organization.url + "."
        }
        return org ? org : ""
      }

      var result = t("Your data will be processed according to the following privacy statement:", 1, { locale: state.consent_language })

      if (state.wizard.purpose) {
        result = result + " " + t("PURPOSE", 1, { locale: state.consent_language }) + ": " + state.wizard.purpose + "."
      }
      result = result + " " + t("LEGITIMACY", 1, { locale: state.consent_language }) + ": "
      if (state.wizard.is_legitimate_use && state.wizard.legitimacy) {
        result = result + state.wizard.legitimacy + "."
      } else {
        result = result + t("By consent.", 1, { locale: state.consent_language }) + " " + t("You may withdraw your consent at any time.", 1, { locale: state.consent_language })
      }
      if (state.wizard.my_organization.name ||
          state.wizard.my_organization.email ||
          state.wizard.my_organization.url) {
        result = result + " " + t("DATA CONTROLLER", 1, { locale: state.consent_language }) + ": " + addOrganization(state.wizard.my_organization)
      }

      if (! state.wizard.recipient_is_my_organization &&
          (state.wizard.data_recipient.name ||
           state.wizard.data_recipient.email ||
           state.wizard.data_recipient.url)) {
        result = result + " " + t("DATA RECIPIENT", 1, { locale: state.consent_language }) + ": " + addOrganization(state.wizard.data_recipient)
      }
      result = result + " " + t("In accordance with the %(privacy_law)s.", 1, { locale: state.consent_language }).replace('%(privacy_law)s', state.data_law)
      //if (!(state.wizard.is_legitimate_use && state.wizard.legitimacy)) {
      //  result = result + " " + t("You may withdraw your consent at any time.")
      //}
      result = result + " " + t("If you have any doubts, please see the contact details above.", 1, { locale: state.consent_language })
      return result
    },
    nav_items: (state) => {
      var items = Object.keys(state.questions)
      if (state.settings.enforce_org == true) {
        items = items.filter(x => x !== "my_organization")
      }
      return items
    }
  },
  actions: {
    setLanguage(lang_code) {
      const { locale } = useI18n({ useScope: 'global' })
      locale.value = lang_code
      this.ui_language = lang_code
    },
    async loadSettings() {
      await axios.get(this.data_endpoint).then(response => {
              this.settings = response.data.settings
              this.default_organization = response.data.default_organization
              this.wizard.my_organization = this.default_organization
              this.wizard = {...this.wizard, ...response.data.wizard}
              if (this.settings.enforce_org) {
                this.wizard.my_organization = this.default_organization
              }
              this.loading_source = false
            })
            .catch(e => {
              console.log(e)
            })
    },
    nextQuestion() {
      let nav_items = this.nav_items
      let current_pos = nav_items.indexOf(this.question_id)
      if (current_pos < nav_items.length-1) {
        this.question_id = nav_items[++current_pos]
      }
    },
    prevQuestion() {
      let nav_items = this.nav_items
      let current_pos = nav_items.indexOf(this.question_id)
      if (current_pos > 0) {
        this.question_id = nav_items[--current_pos]
      }
    }
  }
})
